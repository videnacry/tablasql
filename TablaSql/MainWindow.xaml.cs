﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
namespace TablaSql
{
    //En qué momento podré?
    //Ni idea
    //Con esto hacemos level up
    //Hacia un incierto lugar lleno sin duda de emoción
    /// <summary>
    /// Este programa permite crear y recuperar bases de datos además, agrega tablas a la base de datos "master"
    /// </summary>
    public partial class MainWindow : Window
    {
        static Button test=new Button();
        static SqlConnection test1=new SqlConnection();        
        static Button hola = new Button();       
        static Button newTable = NewButton("New Table", new Thickness(10, 368, 662, 10)),
            newDataBase = NewButton("New Data Base", new Thickness(140, 368, 512, 10)),
            testConnection=NewButton("Test Connection",new Thickness(662,368,10,10));
        static TextBox chainConnection = NewTextBox(new Thickness(10, 40, 192, 348));
        static TextBlock messageAskConnection = NewTextBlock(new Thickness(10, 10, 400f, 378));
        public MainWindow()
        {
            test.Content = "hola";
            hola.Content = "Just another test";
            InitializeComponent();
            messageAskConnection.Text = "Ingrese la instancia sql, ejemplo \"desktop-example/sqlexpress\"";
            newTable.Click += new RoutedEventHandler(NewTable);
            newDataBase.Click += new RoutedEventHandler(CreateDataBase);
            testConnection.Click += new RoutedEventHandler(TestConnection);
            UIElement[] elements = { newTable, chainConnection, newDataBase, messageAskConnection, testConnection };
            Grid grid = AddComponent(elements);
            chainConnection.Text = "Instancia";
            Content = grid;
        }
         Grid AddComponent(UIElement[] objectList)
        {
            Grid grid = new Grid();
            foreach(var obj in objectList)
                grid.Children.Add(obj);
            return grid;
        }
        void NewTable(object sender, RoutedEventArgs e)
        {
            Window newTable = new Window();
            newTable.Height = 450;
            newTable.Width = 800;
            TextBox nameTable = NewTextBox(new Thickness(200, 10, 482, 378));
            TextBox newColumn = NewTextBox(new Thickness(250, 90, 416, 338-40));
            TextBlock askNameTable = NewTextBlock(new Thickness(10, 10, 662-50, 378-80));
            askNameTable.Text = "Ingrese el nombre para la tabla";
            TextBlock askColumnsData = NewTextBlock(new Thickness(10, 50+40, 602-50, 338-80));
            askColumnsData.Text="Ingrese el nombre de la columna, su tipo de dato y con una coma otra columna y su tipo de dato";
            Button createTable = NewButton("Create Table", new Thickness(10,368,668,10));
            UIElement[] elements = { nameTable, createTable, askNameTable, askColumnsData, newColumn };
            Grid grid = AddComponent(elements);
            newTable.Content = grid;
            createTable.Click += new RoutedEventHandler(CreateTable);
            newTable.Show();
            void CreateTable(object send, RoutedEventArgs a)
            {
                string chain = chainConnection.Text, columns = newColumn.Text, table = nameTable.Text;
                DataGrid dataView = new DataGrid();
                dataView.Margin = new Thickness(10, 10, 10, 50);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter($"Select * from {table}", "data source="+chainConnection.Text+";initial catalog = master; integrated security=true");
                DataSet baseCache = new DataSet();
                try
                {
                    sqlAdapter.Fill(baseCache, table);
                    MessageBox.Show("Ya existe una tabla con ese nombre, no está permitido tener 2 tablas con el mismo nombre en una base de datos");
                }
                catch 
                {
                    baseCache.Tables.Clear();
                    sqlAdapter = new SqlDataAdapter($"Create table {table} ({columns}); Select*from {table}", "data source=" + chainConnection.Text + ";initial catalog = master; integrated security=true");
                    sqlAdapter.Fill(baseCache, table);                    
                }
                dataView.ItemsSource = baseCache.Tables[table].AsDataView();
                Button insertRows = NewButton("Finish Changes", new Thickness(682, 378, 10, 10));
                grid.Children.Clear();
                grid.Children.Add(dataView);
                grid.Children.Add(insertRows);
                insertRows.Click += new RoutedEventHandler(Update);  
                sqlAdapter.RowUpdated += new SqlRowUpdatedEventHandler(ErrorFila);
                void Update(object sent, RoutedEventArgs i)
                {
                    SqlCommandBuilder constructor = new SqlCommandBuilder(sqlAdapter);
                    sqlAdapter.Update(baseCache.Tables[table]);
                }
                void ErrorFila(object sending,SqlRowUpdatedEventArgs o)
                {
                    if (o.Errors != null)
                    {
                        o.Status = UpdateStatus.SkipCurrentRow;
                        MessageBox.Show($"{o.Errors.Message} -> {o.StatementType.ToString()} -> {o.Row.ToString()}");
                    }
                }
            }
        }
        void CreateDataBase(object sender,RoutedEventArgs e)
        {
            Window window = new Window();
            TextBlock askDbName =NewTextBlock(new Thickness(10, 10, 662, 378-40)), askDbMaxSize = NewTextBlock(new Thickness(10, 130+120, 682, 258-180)),
                askDbFolder = NewTextBlock(new Thickness(10, 50+40, 602, 338-80)), askDbSize = NewTextBlock(new Thickness(10, 90+80, 702, 298-120));            
            TextBox dbName = NewTextBox(new Thickness(10 + 180, 10, 662 - 180, 378)), dbMaxSize=NewTextBox(new Thickness(10 + 180, 130+120,682 - 180, 258-120)),
                dbFolder = NewTextBox(new Thickness(10 + 150, 50+40, 602 - 180, 338-40)), dbSize = NewTextBox(new Thickness(10 + 150, 90+80, 702 -180, 298-80));
            Button create = NewButton("Crear Base de Datos", new Thickness(10, 378, 602,10));
            TextBox restorer = NewTextBox(new Thickness(200 + 350, 10, 492 - 350, 378)), path = NewTextBox(new Thickness(200 + 350, 50+40, 392 - 350, 338-40));
            TextBlock askRestorer = NewTextBlock(new Thickness(200+180, 10, 492-180, 378-40)), askPath = NewTextBlock(new Thickness(200+180, 50+40, 392-180, 338-80));
            askDbName.Text = "Ingrese el nombre de la base de datos";
            askDbMaxSize.Text = "Ingrese el tamaño máximo que puede alcanzar la base de datos";
            askDbFolder.Text = "Ingrese la dirección en disco que tendrá la base de datos";
            askDbSize.Text = "Ingrese el tamaño base de la base de datos";
            askRestorer.Text = "Ingrese el nombre de la copia de seguridad";
            askPath.Text = "Ingrese la dirección del archivo";
            Button restore = NewButton("Restaurar Base de Datos", new Thickness(200, 378, 392, 10));
            UIElement[] elements = { dbName, dbFolder, dbSize, dbMaxSize, create, restorer, path, restore, askPath, askRestorer, askDbFolder, askDbMaxSize, askDbName, askDbSize };
            Grid grid = AddComponent(elements);
            window.Content = grid;
            window.Height = 450;
            window.Width = 800;
            create.Click += new RoutedEventHandler(Create);
            restore.Click += new RoutedEventHandler(Restore);
            window.Show();
            void Restore(object sending,RoutedEventArgs u)
            {
                SqlDataAdapter adaptador = new SqlDataAdapter($"restore database {restorer.Text} from disk='{path.Text}'",
                    "data source=" + chainConnection.Text + "; integrated security=true");
                DataSet set = new DataSet();
                adaptador.Fill(set);
                restorer.Text = "";
                path.Text = "";
            }
            void Create(object send,RoutedEventArgs a)
            {
                string name = dbName.Text, adress = dbFolder.Text, chain = chainConnection.Text;
                int size = int.Parse(dbSize.Text), maxSize = int.Parse(dbMaxSize.Text);
                using(SqlConnection connection=new SqlConnection("data source=" + chainConnection.Text + ";initial catalog = master; integrated security=true"))
                {
                    connection.Open();
                    connection.StateChange += new StateChangeEventHandler(ChangeState);
                    SqlCommand comando = null;
                    dbFolder.Text = "";
                    dbMaxSize.Text = "";
                    dbName.Text = "";
                    dbSize.Text = "";
                    if (maxSize < 2 || size < 2)
                        MessageBox.Show("El tamaño de la base de datos debe ser mínimo 2");
                    else
                    {
                        try
                        {
                            comando = new SqlCommand($"use {name}", connection);
                            comando.ExecuteNonQuery();
                            MessageBox.Show("Existe una base de datos con el mismo nombre, no está permitido tener más de una bd con el mismo nombre.");
                        }
                        catch
                        {
                            comando = new SqlCommand($"Create database {name} on primary (name='{name}',filename='{adress}{name}.mdf',size={size}MB , maxsize=" +
                            $"{maxSize}MB, filegrowth=10%) log on (name='{name}_log',filename='{adress}{name}.log',size={size / 2}MB,maxSize={maxSize / 2}MB,filegrowth=10%)", connection);
                            try
                            {
                                comando.ExecuteNonQuery();
                            }
                            catch (Exception i)
                            {
                                MessageBox.Show(i.Message);
                            }
                            }
                            finally
                            {
                                connection.Close();
                            }
                    }
                }
                void ChangeState(object sent, StateChangeEventArgs i)
                {
                    MessageBox.Show(i.CurrentState.ToString());
                }
            }
        }
        static TextBlock NewTextBlock(Thickness thickness)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Margin = thickness;
            textBlock.TextWrapping = TextWrapping.Wrap;
            return textBlock;
        }
        static Button NewButton(string content,Thickness thickness)
        {
            Button button = new Button();
            button.Content = content;
            button.Margin = thickness; 
            return button;
        }
        static TextBox NewTextBox(Thickness thickness)
        {
            TextBox textBox = new TextBox();
            textBox.Margin = thickness;
            return textBox;
        }
        static void TestConnection(object sender, RoutedEventArgs args)
        {
            using(SqlConnection connection=new SqlConnection("data source=" + chainConnection.Text + ";initial catalog = master; integrated security=true"))
            {
                try
                {
                    connection.Open();
                    MessageBox.Show("Conexión exitosa");
                    connection.Close();
                }
                catch
                {
                    MessageBox.Show("Conexión fallida");
                }
            }
        }
    }
}
